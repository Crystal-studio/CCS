<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="logo.png">
    <link rel="bookmark" href="logo.png">
    <link href="//fonts.googleapis.com/css?family=Open+Sans|Ubuntu+Mono" rel="stylesheet">
    <link href="//fonts.googleapis.com/earlyaccess/notosansjp.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/earlyaccess/notosanstc.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/earlyaccess/notosanssc.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/earlyaccess/notosanskr.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/mdui/0.4.1/css/mdui.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/layui/css/layui.css">
    <link rel="stylesheet" href="/css/index.css">
    <script src="//v1.hitokoto.cn/?encode=js&select=%23hitokoto" defer></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/mdui/0.4.1/js/mdui.min.js"></script>
    <script src="/layui/layui.js"></script>
