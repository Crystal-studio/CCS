<?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
<title>About - Crystal Studio</title>
<script>var lang = "en";</script>
</head>
<body>

<br><br><br><br>
<div class="layui-container">
    <div class="layui-row">

        <!-- 分隔符 -->
        <div class="separate">
            <fieldset><legend><a name="accordion">Time Axis</a></legend></fieldset>
        </div>

        <br>
        <br>

        <ul class="layui-timeline">
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">June 20, 2015</h3>
                    <p>
                        The Crystal Computer Studio was established<i class="layui-icon"></i>
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">January 2018</h3>
                    <p>
                        Crystal Computer Studio delivered its first question on CTS
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">October 2018</h3>
                    <p>
                        After a series of daunting explorations and a mass of perspiration, Crystal Studio eventually owned its first server
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">June 21, 2018</h3>
                    <p>
                       Crystal Computer Studio convened its first meeting!
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">August 3, 2018</h3>
                    <p>
                        The version 1.0 of Crystal Computer Studio’s website was successfully launched online!
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">August 3, 2018</h3>
                    <p>
                        With the great endeavour of our engineers6., The version 1.0.2.0 of Crystal Studio’s website was successfully launched.
                      <br><br>Update log<br>
                      1. New sideward toolbar and a variety of debugging<br>
                      2. Improving the jump instruction of different language options
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">8. There comes the more interesting future.</h3>
                </div>
            </li>
        </ul>

        <br>
        <br>

        <!-- 分隔符 -->
        <div class="separate">
            <fieldset><legend><a name="accordion">Join us</a></legend></fieldset>
        </div>

        <br>
        <iframe frameborder=0 src="./send/index.php" style="max-width:800px;width:100%;height:700px;margin-left:auto;margin-right:auto;display:block;"></iframe>
    </div>
</div>
<?php include $_SERVER['DOCUMENT_ROOT']."/footer.php"; ?>
