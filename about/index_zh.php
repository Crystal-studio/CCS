<?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
<title>About - Crystal Studio</title>
<script>var lang = "zh";</script>
</head>
<body>

<br><br><br><br>
<div class="layui-container">
    <div class="layui-row">

        <!-- 分隔符 -->
        <div class="separate">
            <fieldset><legend><a name="accordion">时间轴</a></legend></fieldset>
        </div>

        <br>
        <br>

        <ul class="layui-timeline">
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">2015年6月20日</h3>
                    <p>
                        Crystal Computer Studio成立<i class="layui-icon"></i>
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">2018年1月</h3>
                    <p>
                        Crystal Computer Studio第一次在CTS发题
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">2018年10月</h3>
                    <p>
                        经历艰险，Crystal Computer Studio有了第一台自己的服务器！
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">2018年6月21日</h3>
                    <p>
                        Crystal Computer Studio召开第一场组内会议！
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">2018年8月3日</h3>
                    <p>
                        Crystal Computer Studio网站1.0发布，并且成功上线！
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">2018年8月3日</h3>
                    <p>
                        Crystal Computer Studio网站1.0.2.0
                      <br>
                        经过工程师们的努力CCS网站正式上线!
                      <br><br>更新日志<br>
                      1. 新增侧边栏以及修复诸多bug<br>
                      2. 完善多语言版本跳转判断
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">未来将会有更多有趣的事情。</h3>
                </div>
            </li>
        </ul>

        <br>
        <br>

        <!-- 分隔符 -->
        <div class="separate">
            <fieldset><legend><a name="accordion">加入我们</a></legend></fieldset>
        </div>

        <br>
        <iframe frameborder=0 src="./send/index.php" style="max-width:800px;width:100%;height:700px;margin-left:auto;margin-right:auto;display:block;"></iframe>
    </div>
</div>
<?php include $_SERVER['DOCUMENT_ROOT']."/footer.php"; ?>
