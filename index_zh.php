<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>CCS - Crystal Studios</title>
    <link rel="shortcut icon" href="logo.png">
    <link rel="bookmark" href="logo.png">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/mdui/0.4.1/css/mdui.min.css">
    <link rel="stylesheet" href="/layui/css/layui.css">
    <link rel="stylesheet" href="/css/index.css">
    <script src="//v1.hitokoto.cn/?encode=js&select=%23hitokoto" defer></script>
    <script src="/layui/layui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/mdui/0.4.1/js/mdui.min.js"></script>
    <script>var lang = "zh";</script>
</head>
<body>

<!-- 头 -->
<div class="layui-container">
    <div class="layui-row">
        <div class="layui-col-md12 main">
            <!-- 这里四个球 -->
            <span class="circle1"></span>
            <span class="circle2"></span>
            <span class="circle3"></span>
            <span class="circle4"></span>
            <div>
                <img src="//ww1.sinaimg.cn/large/005BYqpgly1ftvs1kzqwyj31kw1kw1ed.jpg" class="logo">
            </div>
            <div class="text-center">
                <p class="fl">
                    <span>Crystal Computer Studio</span>
                    <br>
                    <span class="sl">用代码改变世界</span>
                </p>
            </div>
            <div class="button">
                <a class="layui-btn layui-btn-primary  layui-btn-lg layui-btn-radius" style="width: 170px" href="https://www.cstu.gq" target="_blank"><i
                            class="layui-icon">&#xe609;</i> 主站
                </a>
                <a class="layui-btn layui-btn-warm  layui-btn-lg layui-btn-radius" style="width: 170px" href="./about"><i
                            class="layui-icon">&#xe60b;</i> 关于
                </a>
            </div>
        </div>

        <!-- 介绍等 -->
        <div class="layui-row layui-col-space25">
            <div class="layui-col-md3">
                <a class="widget5" href="https://git.cstu.gq/TURX" target="_blank">
                    <div class="widget5-trending">
                        <img src="./photo/turx.jpg">
                    </div>
                    <div class="widget5-box">
                        <div class="widget5-value">TURX</div>
                        <div class="widget5-label">室长</div>
                    </div>
                    <div class="layui-clear"></div>
                </a>
            </div>


            <div class="layui-col-md3">
                <a class="widget5" href="https://git.cstu.gq/callG" target="_blank">
                    <div class="widget5-trending">
                        <img src="./photo/callg.jpg">
                    </div>
                    <div class="widget5-box">
                        <div class="widget5-value">callG</div>
                        <div class="widget5-label">副室长</div>
                    </div>
                    <div class="layui-clear"></div>
                </a>
            </div>

            <div class="layui-col-md3">
                <a class="widget5" href="https://git.cstu.gq/Magwing" target="_blank">
                    <div class="widget5-trending">
                        <img src="./photo/magwing.jpg">
                    </div>
                    <div class="widget5-box">
                        <div class="widget5-value">Magwing</div>
                        <div class="widget5-label">前室长</div>
                    </div>
                    <div class="layui-clear"></div>
                </a>
            </div>

            <div class="layui-col-md3">

                <a class="widget5" href="https://hap5.top/" target="_blank">
                    <div class="widget5-trending">
                        <img src="./photo/xsy.jpg">
                    </div>
                    <div class="widget5-box">
                        <div class="widget5-value">XSY</div>
                        <div class="widget5-label">WEB工程师</div>
                    </div>
                    <div class="layui-clear"></div>
                </a>
            </div>
        </div>
        <br><br>

        <!-- 分隔符 -->
        <div class="separate">
            <fieldset><legend><a name="accordion">项目</a></legend></fieldset>
        </div>

        <br><br>
        <!-- 项目 -->
        <div class="layui-col-space30">
            <div class="layui-col-md3">
                <div class="box">
                    <div class="box-body">
                        <h2 class="box-zht">
                            <a href="https://git.cstu.gq/CCS/QL/">Quick Launcher</a>
                        </h2>
                        <div class="box-desc">
                            <span>Quick Launcher for Xinyuan Middle School</span>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="footer-reference">
                            <img src="./photo/turx.jpg">
                            <span class="footer-author">TURX</span>
                        </div>
                        <div class="footer-rtime">
                            <span>Jun 10, 2018</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="layui-col-md3">
                <div class="box">
                    <div class="box-body">
                        <h2 class="box-zht">
                            <a href="https://git.cstu.gq/CCS/OI/">OI</a>
                        </h2>
                        <div class="box-desc">
                            <span>Crystal Studio的算法竞赛组</span>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="footer-reference">
                            <img src="./photo/turx.jpg">
                            <span class="footer-author">TURX</span>
                        </div>
                        <div class="footer-rtime">
                            <span>Jun 25, 2018</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="layui-col-md3">
                <div class="box">
                    <div class="box-body">
                        <h2 class="box-zht">
                            <a href="https://git.cstu.gq/CCS/QL_WPF/">QL_WPF</a>
                        </h2>
                        <div class="box-desc">
                            <span>Crystal Studio 基于WPF制作的实用软件</span>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="footer-reference">
                            <img src="./photo/callg.jpg">
                            <span class="footer-author">callG</span>
                        </div>
                        <div class="footer-rtime">
                            <span>Jun 20, 2018</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="layui-col-md3">
                <div class="box">
                    <div class="box-body">
                        <h2 class="box-zht">
                            <a href="https://git.cstu.gq/CCS/CWS">Crystal Web Service</a>
                        </h2>
                        <div class="box-desc">
                            <span>Crystal Studio 的部分网站</span>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="footer-reference">
                            <img src="./photo/turx.jpg" alt="">
                            <span class="footer-author">TURX</span>
                        </div>
                        <div class="footer-rtime">
                            <span>Jun 9, 2018</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- 侧边按钮 -->
        <script>
            layui.use(['util',  'layer'], function(){
                var util = layui.util
                    ,layer = layui.layer;
                //执行
                util.fixbar({
                    bar1: "&#xe614"
                    ,click: function(type){
                        console.log(type);
                        if(type === 'bar1'){
                            layer.open({
                                type: 1,
                                title:'Language',
                                skin: 'layui-layer-molv', //样式类名
                                closeBtn: 1, //显示关闭按钮
                                anim: 2,
                                area: ['250px', '200px'],
                                shadeClose: true, //开启遮罩关闭
                                content: '<ul class="mdui-list">\n' +
                                    '  <li class="mdui-list-item mdui-ripple" onclick="changeLang_newDesign_en();">English</li>\n' +
                                    '  <li class="mdui-list-item mdui-ripple" onclick="changeLang_newDesign_ja();">日本語</li>\n' +
                                    '  <li class="mdui-list-item mdui-ripple" onclick="changeLang_newDesign_zh();">中文</a></li>\n' +
                                    '</ul>'
                            });
                        }
                    }
                });
            });
        </script>


        <!-- footer -->
        <footer>
            <script src="/js/language.js"></script>
            <div class="layui-col-md12">
                <div class="s-footer">
                    <ul>
                        <li><strong>Powered by &copy; Crystal Web Service &amp; XSY</strong></li>
                        <li>Version <?php include $_SERVER['DOCUMENT_ROOT']."/ver.php"; ?></li>
                        <li id="hitokoto">:D 获取中...</li>
                    </ul>
                </div>
            </div>
        </footer>

</body>
</html>
