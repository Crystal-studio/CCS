<?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
<title>CCS - Crystalスタジオグループ</title>
<script>var lang = "ja";</script>
</head>
<body>

<!-- 头 -->
<div class="layui-container">
    <div class="layui-row">
        <div class="layui-col-md12 main">
            <!-- 这里四个球 -->
            <span class="circle1"></span>
            <span class="circle2"></span>
            <span class="circle3"></span>
            <span class="circle4"></span>
            <div>
                <img src="//ww1.sinaimg.cn/large/005BYqpgly1ftvs1kzqwyj31kw1kw1ed.jpg" class="logo">
            </div>
            <div class="text-center">
                <p class="fl">
                    <span>Crystal Computer Studio</span>
                    <br>
                    <span class="sl">ここで夢を叶えています。</span>
                </p>
            </div>
            <div class="button">
                <a class="layui-btn layui-btn-primary  layui-btn-lg layui-btn-radius" style="width: 170px" href="https://www.cstu.gq" target="_blank"><i
                            class="layui-icon">&#xe609;</i> メインサイト
                </a>
                <a class="layui-btn layui-btn-warm  layui-btn-lg layui-btn-radius" style="width: 170px" href="./about"><i
                            class="layui-icon">&#xe60b;</i> について
                </a>
            </div>
        </div>

<!-- 介绍等 -->
<div class="layui-row layui-col-space25">
    <div class="layui-col-md3">
        <a class="widget5" href="https://git.cstu.gq/TURX" target="_blank">
            <div class="widget5-trending">
                <img src="./photo/turx.jpg">
            </div>
            <div class="widget5-box">
                <div class="widget5-value">TURX</div>
                <div class="widget5-label">Chief Executive</div>
            </div>
            <div class="layui-clear"></div>
        </a>
    </div>


    <div class="layui-col-md3">
        <a class="widget5" href="https://git.cstu.gq/callG" target="_blank">
            <div class="widget5-trending">
                <img src="./photo/callg.jpg">
            </div>
            <div class="widget5-box">
                <div class="widget5-value">callG</div>
                <div class="widget5-label">Managing Director</div>
            </div>
            <div class="layui-clear"></div>
        </a>
    </div>

    <div class="layui-col-md3">
        <a class="widget5" href="https://git.cstu.gq/Magwing" target="_blank">
            <div class="widget5-trending">
                <img src="./photo/magwing.jpg">
            </div>
            <div class="widget5-box">
                <div class="widget5-value">Magwing</div>
                <div class="widget5-label">Former Executive</div>
            </div>
            <div class="layui-clear"></div>
        </a>
    </div>

    <div class="layui-col-md3">

        <a class="widget5" href="https://hap5.top/" target="_blank">
            <div class="widget5-trending">
                <img src="./photo/xsy.jpg">
            </div>
            <div class="widget5-box">
                <div class="widget5-value">XSY</div>
                <div class="widget5-label">Web Constructor</div>
            </div>
            <div class="layui-clear"></div>
        </a>
    </div>
</div>
<br><br>

        <!-- 分隔符 -->
        <div class="separate">
            <fieldset><legend><a name="accordion">プロジェクト</a></legend></fieldset>
        </div>

<br><br>
        <!-- 项目 -->
<div class="layui-col-space30">
<div class="layui-col-md3">
        <div class="box">
             <div class="box-body">
                 <h2 class="box-zht">
                     <a href="https://git.cstu.gq/CCS/QL/">Quick Launcher</a>
                 </h2>
                 <div class="box-desc">
                     <span>Quick Launcher for Xinyuan Middle School</span>
                 </div>
             </div>
            <div class="box-footer">
                <div class="footer-reference">
                    <img src="./photo/turx.jpg">
                    <span class="footer-author">TURX</span>
                </div>
                <div class="footer-rtime">
                    <span>平成30年一月10日</span>
                </div>
            </div>
        </div>
</div>

        <div class="layui-col-md3">
            <div class="box">
                <div class="box-body">
                    <h2 class="box-zht">
                        <a href="https://git.cstu.gq/CCS/OI/">OI</a>
                    </h2>
                    <div class="box-desc">
                        <span>Crystal Studioのアルゴリズムグループ</span>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="footer-reference">
                        <img src="./photo/turx.jpg">
                        <span class="footer-author">TURX</span>
                    </div>
                    <div class="footer-rtime">
                        <span>平成30年一月25日</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-col-md3">
            <div class="box">
                <div class="box-body">
                    <h2 class="box-zht">
                        <a href="https://git.cstu.gq/CCS/QL_WPF/">QL_WPF</a>
                    </h2>
                    <div class="box-desc">
                        <span>ＷＰＦでユーティリティプログラムを作りました</span>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="footer-reference">
                        <img src="./photo/callg.jpg">
                        <span class="footer-author">callG</span>
                    </div>
                    <div class="footer-rtime">
                        <span>平成30年一月20日</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-col-md3">
            <div class="box">
                <div class="box-body">
                    <h2 class="box-zht">
                        <a href="https://git.cstu.gq/CCS/CWS">Crystal Web Service</a>
                    </h2>
                    <div class="box-desc">
                        <span>一部Crystal Studiosのサイト</span>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="footer-reference">
                        <img src="./photo/turx.jpg">
                        <span class="footer-author">TURX</span>
                    </div>
                    <div class="footer-rtime">
                        <span>平成30年六月9日</span>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php include $_SERVER['DOCUMENT_ROOT']."/footer.php"; ?>
